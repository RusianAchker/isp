class User:

    def __init__(self, name="Guest"):
        self.id = None
        self.name = name
        self.tasks = list()
        self.history = list()
        self.planned_tasks = list()
