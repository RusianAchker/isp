from console_lib.user_control import UserControl
from console_lib.console import console
from console_lib.myDaemon.my_daemon import MyDaemon
import logging
import sys
import argparse

logging.basicConfig(level=logging.INFO,
                    filename='logging.log')


def create_parser():
    parser = argparse.ArgumentParser()
    sub_parser = parser.add_subparsers(dest='what')

    work_with_tag = sub_parser.add_parser('tag', help='work with tags')

    tag_sub_parser = work_with_tag.add_subparsers(dest='do_tag')

    add_tag = tag_sub_parser.add_parser('add', help='add tag to task')
    add_tag.add_argument('--id', '-i', type=int, help='id task')
    add_tag.add_argument('--tag', '-t', type = str, help='tag to add')

    delete_tag = tag_sub_parser.add_parser('delete', help='delete tag to task')
    delete_tag.add_argument('--id', '-i', type=int, help='id task')
    delete_tag.add_argument('--tag', '-t', type=str, help='tag to delete')

    work_with_task = sub_parser.add_parser('task', help='work with tasks')

    start_daemon = sub_parser.add_parser('start', help='start daemon')
    stop_daemon = sub_parser.add_parser('stop', help='stop daemon')

    show_all = sub_parser.add_parser('show', help='show all info about users')

    task_sub_parser = work_with_task.add_subparsers(dest='do_task')

    add_task = task_sub_parser.add_parser('add', help='add (sub)task')
    add_task.add_argument('--name', '-n', type=str, help='task name')
    add_task.add_argument('--deadline', '-d', type=str, help='task deadline in format hh:mm-dd/mm/yyyy')
    add_task.add_argument('--id', '-i', type=int, default=None, help='id task to add sub task')

    share_task = task_sub_parser.add_parser('share', help='share task')
    share_task.add_argument('--user', '-u', type=int, help='id user to share')
    share_task.add_argument('--task', '-t', type=int, help='id task to share')

    unshare_task = task_sub_parser.add_parser('unshare', help='unshare task')
    unshare_task.add_argument('--user', '-u', type=int, help='id user to unshare')
    unshare_task.add_argument('--task', '-t', type=int, help='id task to unshare')

    edit_task = task_sub_parser.add_parser('edit', help='edit task')
    edit_task.add_argument('--id', '-i', type=int, help='id task to edit')
    edit_task.add_argument('--name', '-n', type=str, default=None, help='name to edit')
    edit_task.add_argument('--deadline', '-d', type=str, default=None, help='deadline to edit')

    delete_task = task_sub_parser.add_parser('delete', help='task to delete')
    delete_task.add_argument('--id', '-i', type=int, help='id task to delete')
    
    search_task = task_sub_parser.add_parser('search', help='task to search')
    search_task.add_argument('--id', '-i', type=int, help='id task to search')

    complete_task = task_sub_parser.add_parser('complete', help='complete task')
    complete_task.add_argument('--id', '-i', type=int, help='id task to search')

    work_with_task = sub_parser.add_parser('user', help='work with users')

    user_subparser = work_with_task.add_subparsers(dest='do_user')

    add_user = user_subparser.add_parser('add', help='add user')
    add_user.add_argument('--name', '-n', type=str, help='name user')

    edit_user = user_subparser.add_parser('edit', help='edit user name')
    edit_user.add_argument('--id', '-i', type=int, help='id user to edit')
    edit_user.add_argument('--name', '-n', type=str, help='new name')

    delete_user = user_subparser.add_parser('delete', help='delete user')
    delete_user.add_argument('--id', '-i', type=int, help='id user to delete')

    switch_user = user_subparser.add_parser('switch', help='swith user')
    switch_user.add_argument('--id', '-i', type=int, help='user id to switch to')

    info_user = user_subparser.add_parser('info', help='info about current user')
    info_user.add_argument('--full', '-f', action='store_const', const=True, default=False,
                           help='bool arg to full info')
    info_user.add_argument('--count', '-c', type=int, default=None)
    info_user.add_argument('--tag', '-t', type=str, help='tag filter', default=None)

    history_user = user_subparser.add_parser('history', help='show user history')
    history_user.add_argument('--delete', '-d', action='store_const', const=True, default=False,
                              help='bool arg to delete history')

    work_with_plan = sub_parser.add_parser('plan', help='work with plans')
    plan_subparser = work_with_plan.add_subparsers(dest='do_plan')

    create_plan = plan_subparser.add_parser('create', help='edit task to plan')
    create_plan.add_argument('--id', '-i', type=int, help='id task to edit')
    create_plan.add_argument('--minute', '-m', type=int, default=0, help='min in period')
    create_plan.add_argument('--hour', '-o', type=int, default=0, help='hours in period')
    create_plan.add_argument('--day', '-d', type=int, default=0, help='days in period')
    create_plan.add_argument('--week', '-w', type=int, default=0, help='weeks in period')

    edit_plan = plan_subparser.add_parser('edit', help='edit plan')
    edit_plan.add_argument('--id', '-i', type=int, help='id plan to edit')
    edit_plan.add_argument('--minute', '-m', type=int, default=0, help='min in period')
    edit_plan.add_argument('--hour', '-o', type=int, default=0, help='hours in period')
    edit_plan.add_argument('--day', '-d', type=int, default=0, help='days in period')
    edit_plan.add_argument('--week', '-w', type=int, default=0, help='weeks in period')
    
    delete_plan = plan_subparser.add_parser('delete', help='edit plan in task')
    delete_plan.add_argument('--id', '-i', type=int, help='id plan to edit')

    return parser


def show_all_info(uc):
    if not uc.user_list:
        print('Список пользователей пуст')
    else:
        for user in uc.user_list:
            console.print_user(user)
            print('')


def main():
    parser = create_parser()
    args = parser.parse_args(sys.argv[1:])

    uc = UserControl()
    uc.load_users()
        
    daemon = MyDaemon(uc, uc.pidfile)
    try:
        if args.what == 'task':
            if args.do_task == 'add':
                if args.id is None:
                    try:
                        uc.add_task(args.name, console.datetime_parser(args.deadline))
                    except ValueError as e:
                        print(e)
                        logging.error(e)
                        return
                    logging.warning('task created')
                elif args.id > 0:
                    try:
                        uc.add_task(args.name, console.datetime_parser(args.deadline), uc.current_user.tasks, args.id)
                    except ValueError as e:
                        print(e)
                        logging.error(e)
                        return
                    logging.warning('subtask created')    
            elif args.do_task == 'delete':
                try:
                    uc.delete_task_id(args.id)
                except ValueError as e:
                    print(e)
                    logging.error(e)
                    return
                logging.warning('task {} deleted'.format(args.id))
            elif args.do_task == 'complete':
                try:
                    uc.complete_task(args.id)
                except ValueError as e:
                    print(e)
                    logging.error('task {} dont comleted'.format(args.id))
                    return
                logging.warning('task {} comleted'.format(args.id))
            elif args.do_task == 'edit':
                try:
                    uc.edit_task_id(args.id, args.name, console.datetime_parser(args.deadline))
                except ValueError as e:
                    print(e) 
                    logging.error(e)
                    return
                logging.warning('task {} edited'.format(args.id))
            elif args.do_task == 'share':
                try:
                    uc.share_task(args.task, args.user)
                except ValueError as e:
                    print(e)
                    logging.error('task {} dont shared'.format(args.task))
                    return
                logging.warning('task {} shared'.format(args.task))
            elif args.do_task == 'unshare':
                try:
                    uc.unshare_task(args.task, args.user)
                except ValueError as e:
                    print(e)
                    logging.error('task {} dont unshared'.format(args.task))
                    return
                logging.warning('task {} unshared'.format(args.task))     
            elif args.do_task == 'search':
                try:
                    temp = uc.find_task_id(args.id)
                    console.print_task(temp)
                except ValueError as e:
                    print(e)
                    logging.error(e)
                    return
                logging.warning('task {} founded'.format(args.id))
            else:
                print('Введите аргумент')
        elif args.what == 'show':
            if not uc.user_list:
                print('Список пользователей пуст')
                return
            show_all_info(uc)
            logging.warning('info showed')
        elif args.what == 'user':
            if args.do_user == 'add':
                try:
                    uc.add_user(args.name)
                except ValueError as e:
                    print(e)
                    logging.error(e)
                    return
                logging.warning('user added')
            elif args.do_user == 'edit':
                try:
                    uc.edit_user(args.id, args.name)
                except ValueError as e:
                    print(e)
                    logging.error(e)
                    return
                logging.warning('user {} edited'.format(args.id))
            elif args.do_user == 'switch':
                try:
                    uc.switch_user(args.id)
                except ValueError as e:
                    print(e)
                    logging.error(e)
                    return
                logging.warning('user {} switched'.format(args.id))
            elif args.do_user == 'delete':
                try:
                    uc.delete_user(args.id)
                except ValueError as e:
                    print(e)
                    logging.error(e)
                    return
                logging.warning('user {} deleted'.format(args.id))
            elif args.do_user == 'info':
                if args.full:
                    if uc.current_user is not None:
                        console.print_user(uc.current_user, args.count, args.tag)
                        logging.warning('showed full user info')
                    else:
                        print('Пользователь не выбран')
                else:
                    if uc.current_user is not None:
                        console.print_short_user(uc.current_user)
                        logging.warning('showed short user info')
                    else:
                        print('Пользователь не выбран')       
            elif args.do_user == 'history':
                if not args.delete:
                    console.print_history(uc.current_user)
                    logging.warning('showed user history')
                else:
                    uc.delete_history()
                    logging.warning('deleted user history')
            else:
                print('Введите аргумет')
                return
        elif args.what == 'start':
            if uc.current_user.planned_tasks:
                daemon.start()
                logging.warning('daemon started')
            else:
                print('Список планов пуст')
        elif args.what == 'stop':
            daemon.stop()
            logging.warning('daemon stoped')
        elif args.what == 'plan':
            if args.do_plan == 'create':
                try:
                    uc.create_plan(args.id, console.delta_parser(args))
                except ValueError as e:
                    print(e)
                    logging.error(e)
                    return
                logging.warning('plan {} created'.format(args.id))
            elif args.do_plan == 'delete':
                try:
                    uc.delete_plan(args.id)
                except ValueError as e:
                    print(e)
                    logging.error(e)
                    return
                logging.warning('plan deleted')
            elif args.do_plan == 'edit':
                try:
                    uc.edit_plan(args.id, console.delta_parser(args))
                except ValueError as e:
                    print(e)
                    logging.error(e)
                    return
                logging.warning('plan {} edited'.format(args.id))
            else:
                print('Неверная команда')
        elif args.what == 'tag':
            try:
                if args.do_tag == 'add':
                    uc.add_tag(args.id, args.tag)
                elif args.do_tag == 'delete':
                    uc.delete_tag(args.id, args.tag)
                else:
                    print('Введите аргумет')
                    return 2
            except ValueError as e:
                print(e)
        else:
            print('Неверная команда')
    except KeyError as e:
        print(e)
        return
    uc.save_users()
    daemon.restart()


if __name__ == '__main__':
    main()
