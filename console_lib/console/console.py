import datetime
import calendar
import re


def print_task(task, tag=None, deep=0):
    """
    Print task.

    Args:
        task (Task) : task obj.
        tag (str) : Tag filter.
        deep (int, optional) : Tab in output.
    """
    if task:
        deadline = (str(task.deadline) + '|' if task.deadline else '')
        string = '\t' * deep + str(task.id) + '|' + str(task.name) + '|' + deadline + str(task.status) + '|' + str(
            task.tags) + (str(task.owner) if task.owner else '')
        if task.is_plan:
            string += '| Period: ' + str(task.delta)
        if tag is None:
            print(string)
        else:
            if tag in task.tags:
                print(string)
            else:
                return
        if task.sub:
            deep += 1
            for sub in task.sub:
                print_task(sub, tag, deep)


def print_user(user, count=None, tag=None):
    """
    Print user's info.

    Args:
        user (User) : User obj.
        count (int) : Count users to print.
        tag (str) : Tag filter.
    """
    if user is None:
        print('Пользователь не выбран')
        return
    string = str(user.id) + '|' + str(user.name)
    print(string)
    print('Tasks:')
    if not user.tasks:
        print('Empty')
    else:
        if count is not None:
            for task in user.tasks:
                if count <= 0:
                    break
                print_task(task, tag)
                count = count - 1
        else:
            for task in user.tasks:
                print_task(task, tag)


def print_history(user):
    """
    Print user's history.

    Args:
        user (User) : User obj.
    """
    string = str(user.id) + '|' + str(user.name)
    print(string)
    print('History:')
    if not user.history:
        print('Empty')
        return
    else:
        for hist in user.history:
            print_task(hist)


def print_short_user(user):
    """
    Print short info about user.

    Args:
        user (User) : User obj.
    """
    string = str(user.id) + '|' + str(user.name)
    print(string)


def delta_parser(args):
    """
    Parse args to datetime.

    Args:
        args.day (int) : Days.
        args.minute (int) : Minutes.
        args.hour (int) : Hours.
        args.week (int) : Weeks.
    Return:
        Delta in datetime type.
    Exceptions:
        raise ValueError : Wrong mins, hours, days, weeks input.
    """
    if args.day < 0:
        raise ValueError('Неверно введены дни')
    elif args.minute < 0:
        raise ValueError('Неверно введены минуты')
    elif args.hour < 0:
        raise ValueError('Неверно введены часы')
    elif args.week < 0:
        raise ValueError('Неверно введены недели')
    return datetime.timedelta(days=args.day, minutes=args.minute, hours=args.hour, weeks=args.week)


def datetime_parser(deadline):
    """
    Parse str to datetime.

    Args:
        deadline (str) : Datetime in format 'hh:mm-dd/mm/yyyy'.
    Returns:
        Deadline in datetime type.
    Exceptions:
        raise ValueError : Wrong datetime input.
    """
    if deadline is None:
        return None
    date = re.split('[:/-]', deadline)
    if len(date) != 5:
        raise ValueError('Неверный формат даты')
    for d in date:
        if d is None or d == '':
            raise ValueError('Введите дату')
    date = [int(x) for x in date]
    if date[3] > 12 or date[3] < 1:
        raise ValueError('Неверный ввод месяца')
    if date[1] > 59 or date[1] < 0:
        raise ValueError('Неверный ввод секунд')
    if date[0] < 0 or date[0] > 23:
        raise ValueError('Неверный ввод часов')
    if date[3] in [1, 3, 5, 7, 8, 10, 12]:
        if date[2] > 31:
            raise ValueError('Неверный ввод дня')
    elif date[3] in [4, 6, 9, 11]:
        if date[2] > 30:
            raise ValueError('Неверный ввод дня')
    else:
        if calendar.isleap(date[4]):
            if date[2] > 29:
                raise ValueError('Неверный ввод дня')
        else:
            if date[2] > 28:
                raise ValueError('Неверный ввод дня(високосный год)')
    return datetime.datetime.strptime(deadline, '%H:%M-%d/%m/%Y')
