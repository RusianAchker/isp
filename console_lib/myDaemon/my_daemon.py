import datetime
import os
import time
import subprocess
from .daemon import Daemon
from trackerweb.tasker.models.constants import Status


class MyDaemon(Daemon):
    def __init__(self, uc, pidfile='/tmp/tracker.pid'):
        super().__init__(pidfile)
        self.tracker = uc
        self.pidfile = pidfile
        self.file_path = 'date/temp.json'

    def run(self):
        while True:
            self._work()
            time.sleep(30)

    def _repeat(self, task):
        while datetime.datetime.now() >= task.deadline:
            task.deadline = task.deadline + task.delta
        new_task = self.tracker.add_task(task.name).id
        self.tracker.delete_task_id(new_task)
        task.id = new_task
        subprocess.check_call(['notify-send', task.name, 'Recreated'])
        self.tracker.save_users(self.file_path)
        os.rename(self.file_path, self.tracker.path)

    def _missed(self, task):
        self.tracker.switch_status(task, Status.DEADLINE_MISSED)
        subprocess.check_call(['notify-send', task.name, 'Deadline missed'])
        self.tracker.save_users(self.file_path)
        os.rename(self.file_path, self.tracker.path)

    def _work(self):
        for task in self.tracker.current_user.planned_tasks:
            if datetime.datetime.now() >= task.deadline:
                self._repeat(task)
        for task in self.tracker.current_user.tasks:
            if task.deadline is not None and not task.is_plan:
                if datetime.datetime.now() >= task.deadline:
                    self._missed(task)
