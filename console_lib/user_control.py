from console_lib.user import User
import json
import jsonpickle
from trackerweb.tasker.models.constants import Status
from console_lib.task import Task
import datetime


class UserControl:
    def __init__(self, path=None):
        self.user_list = list()
        self.current_user = None
        self.last_added_user_id = None
        self.last_added_task_id = 0
        self.pidfile = '/tmp/tracker.pid'
        self.path = "date/users.json"
        if path is not None:
            file = open('config.txt')
            paths = list()
            for line in file:
                path.append(line.split('=')[1])
            self.pidfile = paths[0]
            self.path = paths[1]

    def delete_task_id(self, task_id, lst=None, out=True):
        """
        Method for deleting task.

        Args:
            task_id (int) : Id task to delete.
            lst (list, optional) : List to search task. Defaults to None.
            out (:obj: 'bool', optional) :  Is first deep method. Defaults to True.
        Returns:
            Return True if deleting was successful.
        Exceptions:
            raise ValueError : Task not found.
        """
        if lst is None:
            lst = self.current_user.tasks
        if task_id is None:
            raise KeyError('ID is None')
        for task in lst:
            if task.id == task_id:
                self.delete_owner(task, self.current_user.id)
                if not task.owner:
                    self.switch_status(task, Status.DELETED)
                if task in self.current_user.planned_tasks:
                    self.current_user.planned_tasks.remove(task)
                self.current_user.history.append(task)
                lst.remove(task)
                return True
            elif task.sub:
                if self.delete_task_id(task_id, task.sub, False):
                    return True
        if out:
            raise ValueError('Task not found')

    def switch_status(self, task, status):
        """
        Method for switch status.

        Args:
            task (Task) : Task to switch status.
            status (str) : Status to set.
        """
        task.status = status
        if task.sub:
            for task in task.sub:
                self.switch_status(task, status)

    def find_task_id(self, task_id, lst=None, sub=False):
        """
        Search and return task.

        Args:
            task_id (int) : Id task to search.
            lst (list, optional) : List to search task. Defaults to None.
            sub (:obj: 'bool', optional) :  Is first deep method. Defaults to False  .
        Returns:
            Return founded Task.
        Exceptions:
            raise KeyError : If ID is None.
            raise ValueError : Task not found.
        """
        if task_id is None:
            raise KeyError('ID is None')
        if lst is None:
            lst = self.current_user.tasks
        for task in lst:
            if task.id == task_id:
                return task
            elif task.sub:
                if self.find_task_id(task_id, task.sub, True):
                    return self.find_task_id(task_id, task.sub, True)
        if not sub:
            raise ValueError('Task not found')
            
    def edit_task_id(self, task_id, name=None, deadline=None, lst=None, sub=False):
        """
        Search and edit task.

        Args:
            task_id (int) : Id task to delete.
            name (str, optional) : Name to edit. Defaults to None.
            deadline (:obj: 'datetime', optional) :  Deadline to edit. Defaults to None.
            lst (list, optional) : List to search task. Defaults to None.
            sub (:obj: 'bool', optional) : Is first deep method. Defaults to False.
        Exceptions:
            raise KeyError : ID is None.
            raise ValueError : Task not found.
        """
        if task_id is None:
            raise KeyError('ID is None')
        if lst is None:
            lst = self.current_user.tasks
        for task in lst:
            if task.id == task_id:
                if name is not None:
                    task.name = name
                if deadline is not None:
                    task.deadline = deadline
                return
            elif task.sub:
                if self.edit_task_id(task_id, name, deadline, task.sub, True):
                    return
        if not sub:
            raise ValueError('Task not found')

    def complete_task(self, task_id, lst=None, sub=False):
        """
        Search and complete task.

        Args:
            task_id (int) : Id task to delete
            lst (list, optional) : List to search task. Defaults to None.
            sub (:obj: 'bool', optional) : Is first deep method. Defaults to False.
        Return:
            True if complete was successful.
        Exception: 
            raise KeyError : ID is None.
            raise ValueError : Task not found.
        """
        if task_id is None:
            raise KeyError('ID is None')
        if lst is None:
            lst = self.current_user.tasks
        for task in lst:
            if task.id == task_id:
                self.switch_status(task, Status.COMPLETED)
                lst.remove(task)
                for owner in task.owner:
                    for curr_user in self.user_list:
                        if owner == curr_user.id:
                            curr_user.history.append(task)
                return True
            elif task.sub:
                if self.complete_task(task_id, task.sub, True):
                    return True
        if not sub:
            raise ValueError('Задача не найдена')

    def add_task(self, name, deadline=None, task_id=None, new_task=None):
        """
        Add task or subtask task.

        Args:
            name (str, optional) : Name to add.
            deadline (:obj: 'datetime', optional) :  Deadline to add. Defaults to None.
            task_id (int, optional) : Id task to to add subtask. Defaults to None.
            new_task (Task, optional) : Object of new task. Defaults to None.
        Return:
            Return new task.
        Exception: 
            raise KeyError : Name is None.
        """
        if all(x == ' ' for x in name):
            raise KeyError('Name is None')
        if name is None:
            raise KeyError('Name is None')
        if new_task is None:
                new_task = Task()
                self.last_added_task_id += 1
                new_task.id = self.last_added_task_id
                new_task.name = name
                new_task.status = Status.IN_PROCESS
                new_task.deadline = deadline
                new_task.owner.add(self.current_user.id)
        if task_id is None:
            self.current_user.tasks.append(new_task)
            return new_task
        else:
            subtask = self.find_task_id(task_id)
            if subtask.is_plan:
                raise ValueError('Нельзя добавить подзадачу плану')
            subtask.sub.append(new_task)
            return new_task

    def add_owner(self, task, user_id):
        """
        Add owner to task.

        Args:
            task (Task) : Task to add owner.
            user_id (int) : Owner to add.
        Exception: 
            raise KeyError : Task ID/User ID is None.
            raise ValueError : User not found.
        """
        if task is None:
            raise KeyError('Task is None')
        if user_id is None:
            raise KeyError('User ID is None')
        founded = False
        for user in self.user_list:
            if user.id == user_id:
                founded = True
        if not founded:
            raise ValueError('Пользователь не найден')
        task.owner.add(user_id)
        if task.sub:
            for subtask in task.sub:
                self.add_owner(subtask, user_id)

    def delete_owner(self, task, user_id):
        """
        Delete owner from task.

        Args:
            task (Task) : Task to delete owner.
            user_id (int) : Owner to delete
        Exception: 
            raise KeyError : Task ID/User ID is None.
        """
        if task is None:
            raise KeyError('Task is None')
        if user_id is None:
            raise KeyError('User ID is None')
        task.owner.remove(user_id)
        if task.sub:
            for subtask in task.sub:
                self.delete_owner(subtask, user_id)

    def deadline_task_check(self, tasks):
        """
        Deadline check and switch status on DEADLINE-MISSED.

        Args:
            tasks (list) : List of tasks.
        """
        for task in tasks:
            if task.deadline > datetime.datetime.now():
                self.switch_status(task, Status.DEADLINE_MISSED)
            elif task.sub:
                self.deadline_task_check(task.sub)

    def add_tag(self, task_id, tag):
        """
        Add tag to task.

        Args:
            task_id (int) : Id task to add tag.
            tag (str) : Tag to add
        Exception: 
            raise KeyError : ID/Tag is None.
        """
        if task_id is None:
            raise KeyError('ID is None')
        if tag is None:
            raise KeyError('Tag is None')
        self.find_task_id(task_id).tags.append(tag)

    def delete_tag(self, task_id, tag):
        """
        Delete tag from task.

        Args:
            task_id (int) : Id task to delete tag.
            tag (str) : Tag to delete
        Exception: 
            raise KeyError : ID/Tag is None.
        """
        if task_id is None:
            raise KeyError('ID is None')
        if tag is None:
            raise KeyError('Tag is None')
        self.find_task_id(task_id).tags.remove(tag)

    def switch_user(self, user_id):
        """
        Switch user.

        Args:
            user_id (int) : Id user to switch.
        Return:
            True if switch was successful.
        Exception: 
            raise KeyError : ID is None.
            raise ValueError : User is current.
            raise ValueError : User not found.
        """
        if user_id is None:
            raise KeyError('ID is None')
        if self.current_user is not None:
            if self.current_user.id == user_id:
                raise ValueError('Пользователь уже выбран')
        for user in self.user_list:
            if user.id == user_id:
                self.current_user = user
                return True
        raise ValueError('User not found')

    def add_user(self, name):
        """
        Add user.

        Args:
            name (str) : Name new user's.
        Return:
            New user.
        Exception: 
            raise KeyError : Name is None.
        """
        if all(x == ' ' for x in name):
            raise KeyError('Name is None')
        if name is None or len(name) <= 0:
            raise KeyError('Name is None')
        new_user = User()
        new_user.name = name
        if self.last_added_user_id is None:
            self.last_added_user_id = 1
            new_user.id = self.last_added_user_id
        else:
            self.last_added_user_id += 1
            new_user.id = self.last_added_user_id
        self.current_user = new_user
        self.user_list.append(new_user)
        return new_user
    
    def search_user_id(self, user_id):
        """
        Search user.

        Args:
            user_id (int) : Id user to search.
        Return:
            Founded user or None.
        Exception: 
            raise KeyError : ID is None.
            raise ValueError : User not found.
        """
        if user_id is None:
            raise KeyError('ID is None')
        for user in self.user_list:
            if user.id == user_id:
                return user
        raise ValueError('User not found')

    def delete_user(self, user_id):
        """
        Search and delete user.

        Args:
            user_id (int) : Id user to delete.
        Return:
            True if delete was successful.
        Exception: 
            raise KeyError : ID is None.
            raise ValueError : User not found.
        """
        if user_id is None:
            raise KeyError('ID is None')
        for user in self.user_list:
            if user.id == user_id:
                self.user_list.remove(user)
                if user_id == self.current_user.id:
                    self.current_user = None
                return True
        raise ValueError('User not found')

    def edit_user(self, user_id, name):
        """
        Find and edit user.

        Args:
            user_id (int) : Id user to edit.
            name (str) : New name of user.
        Return:
            True if edit was successful.
        Exception: 
            raise KeyError : Name is None.
            raise ValueError : User not found.
        """
        if name is None:
            raise KeyError('Name is None')
        for user in self.user_list:
            if user.id == user_id:
                user.name = name
                return True
        raise ValueError('User not found')

    def save_users(self, path=None):
        """
        Save users in json.

        Args:
            path (str, optional) : Path to save json. Defaults to None.
        """
        if path is None:
            path = self.path
        with open(path, "w") as file:
            json_users = json.dumps(json.loads(jsonpickle.encode(self)), indent=4)
            file.write(json_users)

    def load_users(self, path=None):
        """
        Load users form json.

        Args:
            path (str, optional) : Path to json.
        Exception: 
            raise Exception : JSson is empty.
        """
        if path is None:
            path = self.path
        with open(path, "r") as file:
            json_file = file.read()
            try:
                tracker = jsonpickle.decode(json_file)
                self.current_user = tracker.current_user
                self.user_list = tracker.user_list
                self.last_added_task_id = tracker.last_added_task_id
                self.last_added_user_id = tracker.last_added_user_id
            except Exception:
                print("json is empty")

    def share_task(self, task_id, to_user_id):
        """
        Share task.

        Args:
            task_id (int) : Id task to share.
            to_user_id (int) : Id user to add task.
        Return:
            True if share was successful.
        Exception: 
            raise KeyError : User ID/ Task ID is None.
            raise ValueError : User not found.
            raise ValueError : Task already shared.
        """
        if task_id is None:
            raise KeyError('Task ID is None')
        if to_user_id is None:
            raise KeyError('User ID is None')
        finded = False
        for user in self.user_list:
            if user.id == to_user_id:
                finded = True
        if not finded:
            raise ValueError('User not found')
        if self.current_user.id != to_user_id:
            self.add_owner(self.find_task_id(task_id), to_user_id)
            for user in self.user_list:
                if user.id == to_user_id:
                    try:
                        self.find_task_id(task_id, user.tasks)
                    except ValueError:
                        new_task = self.find_task_id(task_id)
                        user.tasks.append(new_task)
                        if new_task.is_plan:
                            user.planned_tasks.append(new_task)
                        return True
                    raise ValueError('Task already added')
                    
            raise ValueError('User not found')
        else:
            raise ValueError('You cant add task to myself')
    
    def unshare_task(self, task_id, from_user_id):
        """
        Unshare task.

        Args:
            task_id (int) : Id task to unshare.
            from_user_id (int) : Id user to delete task.
        Exception: 
            raise KeyError : Task ID/User ID is None.
        """
        if task_id is None:
            raise KeyError('Task ID is None')
        if from_user_id is None:
            raise KeyError('User ID is None')
        temp_user = self.current_user.id
        self.switch_user(from_user_id)
        self.delete_task_id(task_id)
        self.switch_user(temp_user)

    def create_plan(self, task_id, delta):
        """
        Create plan.

        Args:
            task_id (int) : Id task to share.
            delta (:obj: 'datetime') : Period to repeat plan.
        Exception: 
            raise KeyError : Task ID is None.
        """
        if task_id is None:
            raise KeyError('Task ID is None')
        founded = self.find_task_id(task_id)
        if founded.sub:
            raise ValueError('Plan cant has subtasks')
        if founded.deadline is None:
            raise ValueError('Deadline is None')
        founded.is_plan = True
        founded.delta = delta
        self.current_user.planned_tasks.append(founded)

    def delete_plan(self, task_id):
        """
        Delete plan.

        Args:
            task_id (int) : Id task to delete plan.
        Returns:
            True if delete was successful.
        Exception: 
            raise KeyError : Task ID is None.
        """
        if task_id is None:
            raise KeyError('Task ID is None')
        temp_task = self.find_task_id(task_id)
        self.current_user.planned_tasks.remove(temp_task)
        temp_task.is_plan = False
        temp_task.delta = None
        return True

    def edit_plan(self, task_id, delta):
        """
        Create plan.

        Args:
            task_id (int) : Id task to share.
            delta (:obj: 'datetime') : Edit period to repeat plan.
        Exception: 
            raise KeyError : Task ID is None.
        """
        if task_id is None:
            raise KeyError('Task ID is None')
        task = self.find_task_id(task_id)
        task.delta = delta

    def delete_history(self):
        """
        Clear user's history.
        """
        self.current_user.history = list()
