import datetime


class Task:

    def __init__(self):
        self.id = 1
        self.name = None
        self.deadline = None
        self.create_time = datetime.datetime.now()
        self.status = None
        self.tags = list()
        self.sub = list()
        self.owner = set()
        self.is_plan = False
        self.delta = None
    