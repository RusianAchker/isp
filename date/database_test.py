import unittest
import console
from console_lib import user_control
import task_control


class DatabaseTest(unittest.TestCase):
    def setUp(self):
        self.user_test = user_control.UserControl()
        self.user = self.user_test.add_user('test')
        self.tc = task_control.TaskContol(self.user_test)

    def test_add_user(self):
        self.user_test.add_user('test1')
        self.assertEqual(self.user_test.user_list[1].name, 'test1')

    def test_edit_user(self):
        self.user_test.edit_user(1, 'new_name')
        self.assertEqual(self.user_test.current_user.name, 'new_name')

    def test_switch_user(self):
        self.user_test.add_user('test1')
        self.user_test.switch_user(1)
        self.assertEqual(self.user_test.current_user.name, 'test')
        
    def test_delete_user(self):
        user = self.user_test.add_user('test1')
        self.user_test.delete_user(2)
        self.assertNotIn(user, self.user_test.user_list)

    def test_search_user_id(self):
        self.user_test.add_user('search')
        user = self.user_test.search_user_id(1)
        self.assertEqual(user.name, 'test')

    def test_add_task(self):
        self.tc.add_task('test_task')
        self.assertIsNotNone(self.tc.find_task_id(1))

    def test_add_subtask(self):
        self.tc.add_task('test_task')
        self.tc.add_task('test_subtask', None, 1)
        self.assertIsNotNone(self.tc.find_task_id(2))

    def test_delete_task(self):
        task = self.tc.add_task('test_task')
        self.tc.delete_task_id(1)
        self.assertNotIn(task, self.user.tasks)

    def test_edit_task(self):
        self.tc.add_task('test_task')
        self.tc.edit_task_id(1, 'edited')
        self.assertEquals(self.tc.tasks[0].name, 'edited')

    def test_complete_task(self):
        self.tc.add_task('test')
        task = self.tc.tasks[0]
        self.tc.complete_task(1)
        self.assertNotIn(task, self.tc.tasks)

    def test_share_task(self):
        user = self.user_test.add_user('test2')
        tc = task_control.TaskContol(self.user_test)
        task = tc.add_task('test_task')
        self.user_test.share_task(task.id, self.user.id)
        self.assertEqual(self.user.tasks[0], self.user_test.user_list[0].tasks[0])

    def test_unshare_task(self):
        user = self.user_test.add_user('test2')
        tc = task_control.TaskContol(self.user_test)
        task = tc.add_task('test_task')
        self.user_test.share_task(task.id, self.user.id)
        self.user_test.unshare_task(task.id, self.user.id)
        self.assertNotIn(task, self.user_test.user_list[0].tasks)
        
    def test_create_plan(self):
        self.tc.add_task('test_task', '22:58-12/5/2018')
        self.user_test.create_plan(1, 1)
        self.assertTrue(self.tc.tasks[0].is_plan)
    
    def test_delete_plan(self):
        self.tc.add_task('test_task', '22:58-12/5/2018')
        self.user_test.create_plan(1, 1)
        self.user_test.delete_plan(1)
        self.assertFalse(self.tc.tasks[0].is_plan)

    def test_edit_plan(self):
        self.tc.add_task('test_task', '22:58-12/5/2018')
        self.user_test.create_plan(1, 1)
        self.user_test.edit_plan(1, 2)
        self.assertEqual(self.tc.tasks[0].delta, 2)

    def test_delete_history(self):
        task = self.tc.add_task('test_task')
        self.tc.delete_task_id(1)
        self.user_test.delete_history()
        self.assertNotIn(task, self.user_test.current_user.history)

    def test_add_owner(self):
        self.user_test.add_user('second')
        tc = task_control.TaskContol(self.user_test)
        task = tc.add_task('New Task')
        tc.add_owner(task, 1)
        self.assertIn(1, task.owner)

    def test_delete_owner(self):
        self.user_test.add_user('second')
        tc = task_control.TaskContol(self.user_test)
        task = tc.add_task('New Task')
        tc.add_owner(task, 1)
        tc.delete_owner(task, 1)
        self.assertNotIn(1, task.owner)

    def test_add_tag(self):
        task = self.tc.add_task('task')
        self.tc.add_tag(task.id, 'test tag')
        self.assertIn('test tag', task.tags)

    def test_delete_tag(self):
        task = self.tc.add_task('task')
        self.tc.add_tag(task.id, 'test tag')
        self.tc.delete_tag(task.id, 'test tag')
        self.assertNotIn('test tag', task.tags)

    def test_datetime_parser(self):
        date = console.datetime_parser('22:53-21/5/2018')
        self.assertEqual(date.strftime('%H:%M-%d/%m/%Y'), '22:53-21/05/2018')

    class arg:
        def __init__(self, day, min, hour, week):
            self.day = day
            self.minute = min
            self.hour = hour
            self.week = week

    def test_time_delta(self):
        ar = self.arg(1, 2, 3, 4)
        date = console.delta_parser(ar)
        self.assertEqual(str(date), '29 days, 3:02:00')
