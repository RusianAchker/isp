from django.db import models
from .constants import PERIODS
from . import Task
from dateutil.relativedelta import relativedelta
from django.utils import timezone


class Plan(models.Model):
    info = models.CharField(max_length=100, default=str)
    owner = models.ForeignKey(
        'Owner',
        related_name='plans',
        null=True,
        on_delete=models.CASCADE)
    enable = models.BooleanField(default=True)
    deadline = models.DateTimeField(null=True,
                                    blank=True)
    period = models.CharField(max_length=7,
                              default=str,
                              help_text='Format: period_type(type: min, h, d, w, m, y)')

    def create_task(self):
        new_task = Task(info=self.info, plan=self, owner=self.owner)
        new_task.save()
        return new_task

    def switch_able(self):
        self.enable = not self.enable
        self.save()

    def get_delta(self):
        delta = self.period.split('_')
        if int(delta[0]) <= 0:
            raise ValueError
        if delta[1] == 'min':
            return relativedelta(minutes=int(delta[0]))
        elif delta[1] == 'h':
            return relativedelta(hours=int(delta[0]))
        elif delta[1] == 'd':
            return relativedelta(days=int(delta[0]))
        elif delta[1] == 'w':
            return relativedelta(weeks=int(delta[0]))
        elif delta[1] == 'm':
            return relativedelta(months=int(delta[0]))
        elif delta[1] == 'y':
            return relativedelta(years=int(delta[0]))
        raise ValueError

    def check_deadline(self):
        if self.deadline is not None:
            if self.deadline < timezone.now() and self.enable:
                self.deadline += self.get_delta()
                self.create_task()
                self.save()

    def __str__(self):
        return str(self.id) + "| " + self.info[:10]
