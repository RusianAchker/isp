from .constants import Status, PRIORITIES
from django.db import models
from django.utils import timezone
from datetime import datetime


class Task(models.Model):
    info = models.CharField(max_length=200,
                            default=str)
    deadline = models.DateTimeField(null=True,
                                    blank=True)
    create_time = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=10,
                              default=Status.IN_PROCESS)
    tags = models.CharField(max_length=100,
                            default=str)
    parent_task = models.ForeignKey('self',
                                    on_delete=models.CASCADE,
                                    related_name='subtasks',
                                    null=True)
    priority = models.PositiveSmallIntegerField(choices=PRIORITIES,
                                                default=2)
    owner = models.ForeignKey('Owner',
                              null=True,
                              on_delete=models.CASCADE,
                              related_name='tasks',
                              related_query_name='task')
    plan = models.ForeignKey('Plan',
                             null=True,
                             on_delete=models.CASCADE)
    is_plan = models.BooleanField(default=False)
    delta = models.DateField(null=True,
                             default=None)
    deleted = models.BooleanField(default=False)

    def add_subtask(self, task):
        self.subtasks.add(task)
        self.save()

    def switch_status(self, status):
        self.status = status
        self.save()

    def revive(self):
        self.status = Status.IN_PROCESS
        self.deleted = False
        for task in self.subtasks.all():
            task.revive()
        self.save()

    def finish(self):
        self.status = Status.COMPLETED
        self.deleted = True
        self.save()
        for task in self.subtasks.all():
            task.finish()

    def delete_task(self):
        self.status = Status.DELETED
        self.deleted = True
        self.save()
        for task in self.subtasks.all():
            task.delete_task()

    def check_deadline(self):
        if self.deadline:
            if self.deadline < timezone.now() and self.status == Status.IN_PROCESS:
                self.status = Status.DEADLINE_MISSED
                self.save()

    def __str__(self):
        return str(self.id) + "| " + self.info[:10]
