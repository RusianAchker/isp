from typing import NamedTuple


class Status:
    DELETED = 'Deleted'
    COMPLETED = 'Complete'
    DEADLINE_MISSED = 'Deadline missed'
    IN_PROCESS = 'In process'


PRIORITIES = [(1, 'Low'), (2, 'Medium'), (3, 'High')]

PERIODS = [(1, 'Day'), (2, 'Week'), (3, 'Month'), (4, 'Year')]
