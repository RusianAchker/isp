from django.db import models
from django.core import exceptions
from .constants import Status


class Owner(models.Model):
    nickname = models.CharField(max_length=15, unique=True, default=str)
    shared_tasks = models.ManyToManyField('Task', related_name='shared')
    my_tasks = models.ManyToManyField('Task', related_name='user_tasks')

    def add_task(self, task):
        self.tasks.add(task)
        self.save()

    def delete_task(self, task_id):
        task = self.find_task(task_id)
        task.status = Status.DELETED

    def find_task(self, task_id):
        def find_subtask(subtask):
            if subtask.exists():
                for task in subtask:
                    if task.sub.filter(pk=task_id):
                        return task.sub.get(pk=task_id)
                    else:
                        find_subtask(task.sub.all())

        if self.tasks.filter(pk=task_id).exists():
            return self.tasks.get(pk=task_id)
        else:
            task = find_subtask(self.tasks.all())
            if task.exists():
                return task
            else:
                raise exceptions.ObjectDoesNotExist

    def add_plan(self, plan):
        self.plans.add(plan)

    def apply_task(self, task):
        self.shared_tasks.add(task)
        self.save()

    def detach_task(self, task):
        self.shared_tasks.remove(task)
        self.save()

    def __str__(self):
        return self.nickname
