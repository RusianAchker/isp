from .control import Tasker
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import redirect, render
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.views.generic import (
    ListView,
    CreateView,
    DetailView,
    UpdateView,
    DeleteView
)
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import TaskCreateForm, TaskShareForm, PlanModelForm
from .models import Task, Plan
from django.http import Http404

db = Tasker()


def signup(request):
    if request.user.is_authenticated:
        return redirect('homepage')
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('homepage')
    else:
        form = UserCreationForm()
    return render(request, 'registration/signup.html', {'form': form})


@login_required
def finish_task(request, pk):
    username = request.user.username
    task = db.find_tasks(username, pk)
    task.finish()
    return redirect('homepage')


@login_required
def revive_task(request, pk):
    username = request.user.username
    task = db.find_tasks(username, pk)
    task.revive()
    return redirect('tasks:archive')


@login_required
def unshare_task(request, pk, name):
    username = request.user.username
    task = db.find_tasks(username, pk)
    owner = db.get_users(name)
    owner.detach_task(task)
    return redirect('homepage')


class TaskListView(LoginRequiredMixin, ListView):
    template_name = 'tasker/task_all.html'
    model = Task
    context_object_name = 'tasks'

    def get_queryset(self):
        username = self.request.user.username
        if self.request.GET.get('order'):
            return db.get_sorted_tasks(username, self.request.GET.get('order'), self.request.GET.get('asc'))
        tasks = db.find_tasks(username)
        return tasks


class TaskArchiveView(LoginRequiredMixin, ListView):
    template_name = 'tasker/task_all.html'
    context_object_name = 'tasks'

    def get_queryset(self):
        username = self.request.user.username
        return db.find_tasks(username, deleted=True, general=False)


class TaskCreateView(LoginRequiredMixin, CreateView):
    form_class = TaskCreateForm
    template_name = 'tasker/add-form.html'
    model = Task

    def get_form_kwargs(self):
        kwargs = super(TaskCreateView, self).get_form_kwargs()
        tasks = db.find_tasks(self.request.user.username)
        kwargs.update({'tasks': tasks})
        return kwargs

    def form_valid(self, form):
        username = self.request.user.username
        new_task = form.save(commit=False)
        new_task.owner = db.get_users(self.request.user.username)
        new_task.save()
        parent_task = form.cleaned_data['parent_task']
        parent_task.add_subtask(new_task) if parent_task else db.add_created(username, 'task', new_task)
        return redirect('homepage')


class TaskDetailView(LoginRequiredMixin, DetailView):
    model = Task
    context_object_name = 'task'
    template_name = 'tasker/task-details.html'

    def get_object(self, queryset=None):
        task = db.find_tasks(username=self.request.user.username, task_id=self.kwargs['pk'])
        return task


class TaskDeleteView(LoginRequiredMixin, DeleteView):
    model = Plan
    template_name = 'tasker/confirm.html'
    success_url = reverse_lazy('tasks:all')

    def get_queryset(self):
        user = self.request.user.username
        return db.find_tasks(user)


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    template_name = 'tasker/add-form.html'
    fields = ['info', 'deadline', 'priority', 'tags']

    def get_success_url(self):
        return reverse_lazy('tasks:detail', args=(self.object.pk,))


class TaskSearchView(TaskListView):
    def get_queryset(self):
        username = self.request.user.username
        query = ' ' + self.request.GET.get('data') + ' '
        tasks = db.find_tasks(username)
        result = list()
        if query:
            for task in tasks:
                if query in (' ' + task.tags + ' '):
                    result.append(task)
        return result


@login_required
def share_task(request, pk):
    users = db.get_users().exclude(nickname=request.user.username)
    form = TaskShareForm(request.POST or None, users=users)

    if request.method == 'POST':
        if form.is_valid():
            username = db.get_users(username=request.user.username)
            user_to = db.get_users(username=form.cleaned_data['user_to'])
            if user_to.nickname == request.user.username:
                raise Http404
            task_to_share = db.find_tasks(username, pk)
            add(username, task_to_share, user_to)
            return redirect('tasks:detail', pk)
    return render(request, 'tasker/task_share.html', {'form': form})


def add(username, task_to_share, user_to):
    user_to.apply_task(task_to_share)
    if task_to_share.subtasks.all().exists():
        for subtask in task_to_share.subtasks.all():
            add(username, subtask, user_to)


class PlanListView(LoginRequiredMixin, ListView):
    template_name = 'tasker/plan_all.html'
    context_object_name = 'plans'

    def get_queryset(self):
        username = self.request.user.username
        return db.find_plans(username)


class PlanCreateView(LoginRequiredMixin, CreateView):
    model = Plan
    form_class = PlanModelForm
    template_name = 'tasker/add-form.html'

    def form_valid(self, form):
        username = self.request.user.username
        new_plan = form.save(commit=False)
        try:
            new_plan.get_delta()
        except ValueError:
            raise Http404
        new_plan.save()
        db.add_created(username, 'plan', new_plan)
        return redirect('plans:all')


class PlanDeleteView(LoginRequiredMixin, DeleteView):
    model = Plan
    template_name = 'tasker/confirm.html'
    success_url = reverse_lazy('plans:all')

    def get_queryset(self):
        user = self.request.user.username
        return db.find_plans(user)


class PlanUpdateView(LoginRequiredMixin, UpdateView):
    model = Plan
    form_class = PlanModelForm
    template_name = 'tasker/add-form.html'

    def get_success_url(self):
        return reverse_lazy('plans:all')


@login_required()
def switch_able(request, pk):
    username = request.user.username
    plan = db.find_plans(username, pk)
    plan.switch_able()
    return redirect('plans:all')