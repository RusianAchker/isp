from .views import db
from django.http import Http404


class TaskerMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        user = request.user.username
        if user:
            for task in db.find_tasks(user, general=False):
                task.check_deadline()
            for plan in db.find_plans(user):
                plan.check_deadline()
        return self.get_response(request)
