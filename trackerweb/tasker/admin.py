from django.contrib import admin
from .models import Task, Owner, Plan

admin.site.register(Task)
admin.site.register(Owner)
admin.site.register(Plan)
