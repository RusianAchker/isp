from .models import (
    Task,
    Plan,
)
from django import forms
from .models.constants import PRIORITIES


class TaskCreateForm(forms.ModelForm):
    parent_task = forms.ModelChoiceField(empty_label='Do not make subtask',
                                         queryset=Task.objects.none(),
                                         required=False)
    deadline = forms.DateTimeField(required=False,
                                   input_formats=['%H:%M %d-%m-%Y'],
                                   help_text="Format: HH:MM dd-mm-YYYY")
    priority = forms.ChoiceField(choices=PRIORITIES)
    tags = forms.CharField(required=False,
                           max_length=100)

    def __init__(self, tasks, *args, **kwargs):
        super(TaskCreateForm, self).__init__(*args, **kwargs)
        if tasks:
            self.fields['parent_task'].queryset = tasks

    class Meta:
        model = Task
        fields = ['info', 'deadline', 'tags', 'priority']


class TaskShareForm(forms.Form):
    user_to = forms.CharField(required=True,
                              max_length=10)

    def __init__(self, *args, **kwargs):
        self._users = kwargs.pop('users')
        super(TaskShareForm, self).__init__(*args, **kwargs)


class PlanModelForm(forms.ModelForm):
    deadline = forms.DateTimeField(input_formats=['%H:%M %d-%m-%Y'])

    class Meta:
        model = Plan
        fields = ['info', 'deadline', 'period']
