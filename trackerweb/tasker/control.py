from django.db.models import Q, F
from django.core.exceptions import ObjectDoesNotExist
from functools import reduce
import operator
from .models import Task, Owner
from django.shortcuts import get_object_or_404


class Tasker:
    def find_tasks(self, username, task_id=None, tags=None, deleted=None, general=True):
        user = self.get_users(username)
        if task_id:
            try:
                return user.tasks.get(pk=task_id)
            except ObjectDoesNotExist:
                return user.shared_tasks.get(pk=task_id)
        elif tags:
            result = user.tasks.filter(reduce(operator.and_, (Q(tags__contains=tag) for tag in tags)))
        elif deleted:
            result = user.tasks.filter(deleted=True)
        else:
            result = user.tasks.filter(deleted=False) | user.shared_tasks.filter(deleted=False)
        return result.filter(parent_task__isnull=True) if general else result

    def find_plans(self, username, plan_id=None):
        user = self.get_users(username)
        return user.plans.get(pk=plan_id) if plan_id else user.plans.all()

    def add_task(self, username, info, deadline=None, tags=None, parent_task_id=None):
        task = Task(info=info, deadline=deadline, tags=tags)
        user = self.get_users(username)
        if parent_task_id:
            parent_task_id = user.tasks.get(pk=parent_task_id)
            task.save()
            parent_task_id.add_subtask(task)
        else:
            task.save(username)
        user.add_task(task)

    def add_created(self, username, type, object):
        user = self.get_users(username)
        if type == 'task':
            user.add_task(object)
        elif type == 'plan':
            user.add_plan(object)
        else:
            raise AttributeError

    def complete_task(self, username, task_id):
        user = self.get_users(username)
        user.find_task(task_id).finish()

    def revive_task(self, username, task_id):
        user = self.get_users(username)
        user.find_task(task_id).revive()

    @staticmethod
    def get_users(username=None):
        if username:
            user = get_object_or_404(Owner, nickname=username)
            return user
        else:
            return Owner.objects.all()

    @staticmethod
    def add_user(username):
        Owner.objects.create(username)

    @staticmethod
    def delete_user(username):
        Owner.objects.get(nickname=username).delete()

    def get_sorted_tasks(self, username, field, asc, general=True):
        query = self.find_tasks(username, general=general)
        if asc == 'desc':
            return query.order_by(F(field).desc(nulls_last=True))
        else:
            return query.order_by(F(field).asc())