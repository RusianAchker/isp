from django.conf.urls import (
    url,
    include
)
from django.views.generic import RedirectView
import django.contrib.auth.views as auth_views
from django.contrib import admin
from . import views

task_patterns = [
    url(r'^$', views.TaskListView.as_view(), name='all'),
    url(r'^new/$', views.TaskCreateView.as_view(), name='new'),
    url(r'^archive/$', views.TaskArchiveView.as_view(), name='archive'),
    url(r'^(?P<pk>\d+)/$', views.TaskDetailView.as_view(), name='detail'),
    url(r'^(?P<pk>\d+)/finish/$', views.finish_task, name='finish'),
    url(r'^(?P<pk>\d+)/delete/$', views.TaskDeleteView.as_view(), name='delete'),
    url(r'^(?P<pk>\d+)/share/$', views.share_task, name='share'),
    url(r'^(?P<pk>\d+)/restore/$', views.revive_task, name='revive'),
    url(r'^(?P<pk>\d+)/edit/$', views.TaskUpdateView.as_view(), name='edit'),
    url(r'^(?P<pk>\d+)/unshare/(?P<name>\w+)/$', views.unshare_task, name='unshare'),
    url(r'^search/$', views.TaskSearchView.as_view(), name='search'),
]

plan_patterns = [
    url(r'^$', views.PlanListView.as_view(), name='all'),
    url(r'^new/$', views.PlanCreateView.as_view(), name='new'),
    url(r'^(?P<pk>\d+)/delete/$', views.PlanDeleteView.as_view(), name='delete'),
    url(r'^(?P<pk>\d+)/edit/$', views.PlanUpdateView.as_view(), name='edit'),
    url(r'^(?P<pk>\d+)/switch/$', views.switch_able, name='switch'),
]

urlpatterns = [
    url(r'^$', RedirectView.as_view(url='tasks'), name='homepage'),
    url(r'^login/$', auth_views.LoginView.as_view(redirect_authenticated_user=True), name='login'),
    url(r'^logout/$', auth_views.logout, name='logout'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^tasks/', include(task_patterns, namespace='tasks')),
    url(r'^plans/', include(plan_patterns, namespace='plans')),
    url(r'^admin/', admin.site.urls),
]
