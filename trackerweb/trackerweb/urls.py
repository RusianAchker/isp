from django.conf.urls import (
    url,
    include,
)

urlpatterns = [
    url(r'^', include('tasker.urls')),
]
